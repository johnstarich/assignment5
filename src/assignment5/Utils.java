package assignment5;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides useful methods for a static import.
 * @author johnstarich
 */
public class Utils
{
	public static void println()
	{
		System.out.println();
	}
	
	public static void println(Object o)
	{
		System.out.println(o);
	}
	
	public static void print(Object o)
	{
		System.out.print(o);
	}
	
	/**
	 * Prints every item in the list on new lines.
	 * @param items the list to print
	 */
	public static <T> void println(List<T> items)
	{
		items.stream().forEach(item -> println(item));
	}
	
	/**
	 * Prints every item in the list.
	 * @param items the list to print
	 */
	public static <T> void print(List<T> items)
	{
		items.stream().forEach(item -> print(item));
	}
	
	/**
	 * Prints every item in the list concatenated by the separator.
	 * @param items the list to print
	 */
	public static <T> void print(List<T> items, String separator)
	{
		items.stream().forEach(item ->
		{
			print(item);
			print(separator);
		});
	}
	
	/**
	 * Prints every item in the list on new lines.
	 * @param items the list to print
	 */
	public static <T> void println(Set<T> items)
	{
		items.stream().forEach(item -> println(item));
	}
	
	/**
	 * Prints every item in the list.
	 * @param items the list to print
	 */
	public static <T> void print(Set<T> items)
	{
		items.stream().forEach(item -> print(item));
	}
	
	/**
	 * Prints every item in the list concatenated by the separator.
	 * @param items the list to print
	 */
	public static <T> void print(Set<T> items, String separator)
	{
		items.stream().forEach(item ->
		{
			print(item);
			print(separator);
		});
	}
	
	/**
	 * Prints every key value pair in the map, separated by newlines.
	 * @param entries the list to print
	 */
	public static <A, B> void print(Map<A, B> entries)
	{
		print(entries, "\n");
	}
	
	/**
	 * Prints every key value pair in the map, separated by the entry separator string.
	 * @param entries the list to print
	 * @param entrySeparator the string separating the entries
	 */
	public static <A, B> void print(Map<A, B> entries, String entrySeparator)
	{
		print(entries, entrySeparator, " -> ");
	}
	
	/**
	 * Prints every key value pair in the map separated by the
	 * key-value separator string and the entry separator string.
	 * @param entries the list to print
	 * @param keyValueSeparator the string separating the key and value
	 * @param entrySeparator the string separating the entries
	 */
	public static <A, B> void print(Map<A, B> entries, String entrySeparator, String keyValueSeparator)
	{
		entries.entrySet().stream().forEach(entry ->
		{
			print(entry.getKey());
			print(keyValueSeparator);
			print(entry.getValue());
			print(entrySeparator);
		});
	}
	
	/**
	 * Creates a list of random positive integers with the specified size.
	 * @param size how many numbers to generate
	 * @return the list
	 */
	public static List<Integer> randomUnsignedIntList(int size)
	{
		Random rand = new Random(15485863);
		List<Integer> list = new ArrayList<Integer>(size);
		for(int i = 0; i < size; i += 1)
			list.add(rand.nextInt(Integer.MAX_VALUE) + 1);
		return list;
	}
	
	/**
	 * Reads in a file with the specified file name and returns it in a list of strings.
	 * @param fileName the file name
	 * @return the list of strings, one item per line
	 */
	public static List<String> readFile(String fileName)
	{
		try
		{
			return Files.readAllLines(FileSystems.getDefault().getPath(fileName));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Convert a list of Strings to a list of integers, filtering out invalid integers along the way.
	 * @param list the list to convert
	 * @return the converted list
	 */
	public static List<Integer> convertToIntList(List<String> list)
	{
		return list.parallelStream()
			.map(string ->
			{
				try { return Integer.valueOf(string); }
				catch(NumberFormatException e) { return null; }
			})
			.filter(number -> number != null)
			.sequential()
			.collect(Collectors.toList());
	}
	
	/**
	 * Execute the given function and its code while muting standard output.
	 * @param x the function to run
	 */
	public static void silentlyExecute(Runnable x)
	{
		PrintStream original = System.out;
		System.setOut(new PrintStream(new OutputStream()
		{
			public void write(int b) {}
		}));
		x.run();
		System.setOut(original);
	}
}
