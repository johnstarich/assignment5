package assignment5;

/**
 * Thrown when an invalid word if encountered when computing a word ladder.
 * @author Robert Ly
 */
public class InvalidWordException extends NoSuchLadderException
{
	private static final long serialVersionUID = 1L;

    public InvalidWordException(String message)
    {
        super(message);
    }

    public InvalidWordException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
