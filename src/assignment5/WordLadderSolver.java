package assignment5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Computes word ladders given a start and end word. It must be created with a dictionary of valid words.
 * Section 16005
 * @author John Starich (js68634), Nirali Desai (nmd466), Robert Ly (rwl539)
 */
public class WordLadderSolver implements Assignment5Interface
{
	public static final int WORD_LENGTH = 5;
	private Dictionary dictionary;

	/**
	 * Creates a word ladder solver which uses the provided dictionary as legal words.
	 * @param dictionary the legally binding words
	 */
	public WordLadderSolver(Dictionary dictionary)
	{
		this.dictionary = dictionary;
	}

	@Override
	public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException 
	{
		if(! dictionary.doesWordExist(startWord) || ! dictionary.doesWordExist(endWord))
			throw new InvalidWordException("The start or end word is not in the dictionary");
		if(startWord.length() == WORD_LENGTH && endWord.length() == WORD_LENGTH && startWord.equals(endWord))
		{
			ArrayList<String> list = new ArrayList<String>();
			list.add(startWord);
			return list;
		}
		ArrayList<String> list = new ArrayList<String>();
		list.add(startWord);
		return makeLadder(startWord, endWord, list, -1, new HashSet<String>(100, 0.5f));
	}

	/**
	 * Get the index that is different between the two provided words.
	 * @param word1 first word to check
	 * @param word2 second word to check against
	 * @return the index at which the words differ first
	 */
	public Integer getChangedIndex(String word1, String word2)
	{
		for(int i = 0; i < word1.length(); i += 1)
			if(word1.charAt(i) != word2.charAt(i))
				return i;
		return null;
	}

	/**
	 * Make a ladder beginning at the currentWord and completing at the endWord.
	 * Exclude any previousWords from analysis and ignore words which do not differ at the index specified.
	 * @param currentWord the word to start at
	 * @param endWord the word to stop at
	 * @param previousWords any previous words identified in analysis
	 * @param previousChangedIndex the index to ignore when finding one-off words
	 * @return the word ladder linking the start and end words
	 * @throws NoSuchLadderException thrown when no ladder for the given bounds and restrictions can be found
	 */
	public List<String> makeLadder(String currentWord, String endWord, List<String> previousWords, int previousChangedIndex, HashSet<String> triedWords) throws NoSuchLadderException
	{
		if(Dictionary.isOneLetterOff(currentWord, endWord))
		{
			previousWords.add(endWord);
			return previousWords;
		}

		Dictionary possibleWords = dictionary.oneLetterOff(currentWord, previousChangedIndex);
		possibleWords.removeAllWords(previousWords);
		possibleWords.removeAllWords(triedWords);
		for(String word : possibleWords)
		{
			triedWords.add(word);
			previousWords.add(word);
			try
			{
				return makeLadder(word, endWord, previousWords, getChangedIndex(word, currentWord), triedWords);
			}
			catch(NoSuchLadderException e)
			{
				previousWords.remove(word);
			}
		}
		throw new NoSuchLadderException("No working ladders remaining");
	}

	@Override
	public boolean validateResult(String startWord, String endWord, List<String> wordLadder) 
	{
		throw new UnsupportedOperationException("Not implemented yet!");
	}
}
