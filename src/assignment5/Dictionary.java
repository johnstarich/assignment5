package assignment5;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Stores words from a dictionary file and any subsets. 
 * @author Nirali Desai
 */
public class Dictionary implements Iterable<String>
{
	private LinkedHashSet<String> dictionaryfile;
	
	/**
	 * Creates a dictionary from the contents of the file with the given name.
	 * @param filename the provided dictionary file's name
	 */
	public Dictionary(String filename)
	{
		dictionaryfile = makeDictionary(filename);
	}
	
	/**
	 * Creates a dictionary from the given list of words.
	 * @param words the words to create this dictionary with
	 */
	public Dictionary(Set<String> words)
	{
		dictionaryfile = new LinkedHashSet<String>(words);
	}
	
	/**
	 * Makes new dictionary with all words from file
	 * @return a list of strings with all the words
	 */
	public LinkedHashSet<String> makeDictionary(String filename)
	{
		List<String> list = Utils.readFile(filename);
	
		for (int i=0; i < list.size(); i++)
		{
			String tempsub = list.get(i).substring(0,WordLadderSolver.WORD_LENGTH);
			list.set(i,tempsub);
		}

		Iterator<String> iter = list.iterator();
        while(iter.hasNext())
        {
            if(iter.next().contains("*"))
                iter.remove();
        }
        
		return new LinkedHashSet<String>(list);
	}
	
	/**
	 * Prints the dictionary
	 */
	public void printDictionary()
	{
		for(String word : dictionaryfile)
		{
			System.out.println(word);
		}
	}
	
	/**
	 * Removes all words in the provided collection from this dictionary
	 * @param words the collection of words to remove
	 */
	public void removeAllWords(Collection<String> words)
	{
		dictionaryfile.removeAll(words);
	}
	
	/**
	 * Removes a certain word from the dictionary
	 * @param word the word to remove
	 */
	public void removeWord(String word)
	{
		dictionaryfile.remove(word);
	}
	
	/**
	 * Checks if word exists in dictionary
	 * @param word the word to check
	 * @return true if words exists, false otherwise
	 */
	public boolean doesWordExist(String word)
	{
		return dictionaryfile.contains(word);
	}
	
	/**
	 * Returns whether or not the word is exactly one letter off
	 * of the target but not at the specified index.
	 * @param word the word to check against the target
	 * @param target the word to find the different letter in
	 * @param index the index the words are not allowed to differ in
	 * @return true if exactly one letter off but not at the index, false otherwise
	 */
	public static boolean isOneLetterOff(String word, String target, int index)
	{
     	int sameletter = -1;
    	for(int i = 0; i < word.length(); i += 1)
    	{
    		if (word.charAt(i) != target.charAt(i))
    		{
    			if(sameletter >= 0)
    				return false;
    			sameletter = i;
    		}
    	}
    	return sameletter != index;
    }
    
	/**
	 * Returns whether or not the word is exactly one letter off
	 * of the target but not at the specified index.
	 * @param word the word to check against the target
	 * @param target the word to find the different letter in
	 * @return true if exactly one letter off but not at the index, false otherwise
	 */
	public static boolean isOneLetterOff(String word, String target)
	{
    	boolean sameletter = false;
    	for(int i = 0; i < word.length(); i += 1)
    	{
    		if (word.charAt(i) != target.charAt(i))
    		{
    			if(sameletter)
    				return false;
    			sameletter = true;
    		}
    	}
    	return sameletter;
    }
    
	/**
	 * Returns a new dictionary that contains a list of words that are one letter off from input word
	 * @param word the word from which list is generated
	 * @param index the index where the one letter off is not desired
	 * @return new dictionary with the list of one letter off words
	 */
	public Dictionary oneLetterOff(String word, int index)
	{
		HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
		for(String s : dictionaryfile) {
			if(isOneLetterOff(s, word, index)) {
				int i;
				for(i = 0; i < word.length(); i++)
					if(s.charAt(i) != word.charAt(i))
						break;
					tempMap.put(s, Math.abs(i - index));
			}
		}

		Dictionary dictionary = new Dictionary(
			tempMap.entrySet().stream()
				.sorted(Comparator.comparingInt(entry -> entry.getValue()))
				.map(entry -> entry.getKey())
				.collect(Collectors.toSet())
		);
		
		return dictionary;
	}

	/*
	 * Potentially useful sort algorithms but currently only slows us down.
	public ArrayList<String> sortByDifferenceLocation(String target, int index)
	{
		return sort(Comparator.comparingInt(word ->
		{
			for(int i = 0; i < word.length(); i++)
			{
				if(target.charAt(i) != word.charAt(i))
				{
					if(i - index > 0)
						return i - index;
					return (i - index) * word.length();
				}
			}
			return 1;
		}));
	}
	
	public ArrayList<String> sort(Comparator<String> comparator)
	{
		ArrayList<String> list = new ArrayList<String>(dictionaryfile);
		list.sort(comparator);
		return list;
	}
	*/

	@Override
	public Iterator<String> iterator()
	{
		return dictionaryfile.iterator();
	}
	
}

