package assignment5;

import java.util.List;

import assignment5.StopWatch;
import static assignment5.Utils.*;

/**
 * Assignment 5 driver class
 * @author Robert Ly 
 */
public class A5Driver
{
	/**
	 * Read file with start and end words, compute and print word ladders, time execution
	 * @param name of file with start and end words
	 * @param name of dictionary file
	 */
    public static void main(String[] args) throws Exception
    {
        // Create a word ladder solver object
    	if(args.length < 1)
        	throw new Exception("Input file not specified.");
    	if(args.length < 2)
        	throw new Exception("Dictionary file not specified.");
    	
    	String inputFileName = args[0];
    	String dictionaryFileName = args[1];
    	
        // Create a word ladder solver object
        Assignment5Interface wordLadderSolver = new WordLadderSolver(new Dictionary(dictionaryFileName));
        
        StopWatch timer = new StopWatch();
        // Read file in
        List<String> lines = readFile(inputFileName);
        timer.start();
        // for each line in the file call compute ladder
        for(String line: lines)
        {
        	String[] words = line.split("\\s+"); //split on whitespace
        	print("For the input words \"" + words[0] + "\" and \"" + words[1] + "\"");
	        try 
	        {
	        	
	            List<String> result = wordLadderSolver.computeLadder(words[0], words[1]);
	            //boolean correct = wordLadderSolver.validateResult(words[0], words[1], result);
        		
        		println(" the following word ladder was found ");
	        	for(String word: result)
	        	{
	        		println(word);
	        	}
	        	println("**********");
	        } 
	        catch (InvalidWordException e)
	        {
	        	//e.printStackTrace();
	        	println("\nAt least one of the words " + words[0] + " and " + words[1] + " are not legitimate 5-letter words from the dictionary.");
	        }
	        catch (NoSuchLadderException e) 
	        {
	            //e.printStackTrace();
	            println("\nThere is no such ladder between \"" + words[0] + "\" and \"" + words[1] + "\"!");
	        }
	        
	        println();
        }
        timer.stop();
        
        println("Time elapsed in nanoseconds: " + timer.getElapsedTime());
		println("Time elapsed in seconds: " + timer.getElapsedTime()/StopWatch.NANOS_PER_SEC + "\n");
    }
}
